//
//  PFAArtistGallerySharedStore.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFAArtistGallerySharedStore.h"
#import "PFAArtistGalleryData.h"


@interface PFAArtistGallerySharedStore()
@end

@implementation PFAArtistGallerySharedStore

- (id)init
{
    self = [super init];
    
    if (self)
    {

    }
    
    return self;
}

- (NSMutableArray *)categories
{
    if (!_categories)
    {
        _categories = [[NSMutableArray alloc] init];
    }
    return _categories;
}

- (NSMutableDictionary *)galleries
{
    if (!_galleries)
    {
        _galleries = [[NSMutableDictionary alloc] init];
    }
    return _galleries;
}

+ (PFAArtistGallerySharedStore *)sharedStore
{
    static PFAArtistGallerySharedStore *sharedStore = nil;
    if (!sharedStore)
    {
        sharedStore = [[super allocWithZone:nil] init];
    }
    
    return sharedStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedStore];
}

@end

//
//  PFAUploadViewController.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/13/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFAUploadViewController.h"
#import "PFAPostCard.h"
#import "PFARecipient.h"
#import "PFASender.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "PFAPostCardStore.h"
#import "PayPalMobile.h"
#import "GAI.h"
#import "util.h"


@interface PFAUploadViewController () <PayPalPaymentDelegate>
@property (nonatomic, strong) NSURLConnection *uploadCardDataConnection;
@property (nonatomic, strong) NSURLConnection *uploadPaymentData;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSString *postcardData;
@property (nonatomic, strong) NSString *orderNumber;
@property (nonatomic, strong) UIAlertView *cardSavedAlert;
@property (nonatomic, strong) UIAlertView *saveAsAlert;
@property (nonatomic, strong) UIImage *socialImage;
@property (nonatomic, setter = isShareViewOpen:) BOOL *shareViewOpen;
@property (nonatomic, setter = didPhotoUploadSuccessfully:) BOOL photoUploadedSuccessfully;
@property (nonatomic, setter = isPaymentComplete:) BOOL paymentComplete;
@end

@implementation PFAUploadViewController
{
    BOOL firstLoad;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    firstLoad = YES;
    
    if (self.freeCard)
        self.status = @"paid";
    else
        self.status = @"unpaid";
    
    NSLog(@"%@", self.status);
    
    // google anaylitics
    self.trackedViewName = @"Upload Screen";
    
    self.promoCode = [self.promoCode stringByReplacingOccurrencesOfString:@"promo=" withString:@""];
    
    // setup tumblr
    //[TMAPIClient sharedInstance].OAuthConsumerKey = @"B6QqdGMoaKNp6TTXeFW2P10cpgoKgny0KWNMpn4lEQ8MjahSIf";
    //[TMAPIClient sharedInstance].OAuthConsumerSecret = @"bQ7gICLU0AWzS2106g626BXPJYrrMbvnG5V7MREGtfyiWLpodg";
    
    UIFont *buttonFont = [UIFont fontWithName:@"League Gothic" size:24];
    
    [self.uploadLabel setFont:buttonFont];
    [self.rateAppButton.titleLabel setFont:buttonFont];
    [self.returnHome.titleLabel setFont:buttonFont];
    [self.saveCardButton.titleLabel setFont:buttonFont];
    [self.retryButton.titleLabel setFont:buttonFont];
    [self.cancelToHome.titleLabel setFont:buttonFont];
    [self.saveCardRetryButton.titleLabel setFont:buttonFont];
    
    if ((self.postcard.image.size.width > 800) || (self.postcard.image.size.height > 800))
    {
        
        CGFloat imgWidth = 800;
        CGFloat imgHeight = (800 / self.postcard.image.size.width) * self.postcard.image.size.height;
        
        CGSize scaledSize = CGSizeMake(imgWidth, imgHeight);
        
        NSLog(@"%f, %f", scaledSize.width, scaledSize.height);
        
        self.socialImage = [util imageWithImage:self.postcard.image scaledToSize:scaledSize];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    [PayPalPaymentViewController setEnvironment:PayPalEnvironmentProduction];
    [PayPalPaymentViewController prepareForPaymentUsingClientId:@"Afh4khAAWdB__GMTkaBzVTXimZOWcxOYx4-60hDER567_EFYydX5N8fmR3Fv"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIApplication* application = [UIApplication sharedApplication];
    if (application.statusBarOrientation == UIInterfaceOrientationPortrait)
    {
        UIViewController *c = [[UIViewController alloc]init];
        [self presentViewController:c animated:NO completion:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
    if (firstLoad)
    {
        [self sendPostcard:self.postcard];
        firstLoad = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)rateAppPressed:(UIButton *)sender
{
    NSString *url = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=589577542&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (IBAction)returnHomePressed:(UIButton *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)saveCardButtonPressed:(UIButton *)sender
{
    if (self.isSavedCard)
    {
        self.saveAsAlert = [[UIAlertView alloc] initWithTitle:@"Save As" message:@"Would you like to update this saved card?" delegate:self cancelButtonTitle:@"Update card" otherButtonTitles:@"Save as new", nil];
        
        [self.saveAsAlert show];
    }
    else
    {
        [[PFAPostCardStore sharedStore] addPostcard:self.postcard];
        self.cardSavedAlert = [[UIAlertView alloc]  initWithTitle:[NSString stringWithFormat:@"Postcard saved"]
                                                          message:[NSString stringWithFormat:@"Your postcard has been saved"]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [self.cardSavedAlert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.saveAsAlert)
    {
        if (buttonIndex == 0)
        {
            [[PFAPostCardStore sharedStore] replacePostcardAtIndex:self.savedCardIndex withPostcard:self.postcard];
            self.cardSavedAlert = [[UIAlertView alloc]  initWithTitle:[NSString stringWithFormat:@"Postcard updated"]
                                                              message:[NSString stringWithFormat:@"Your postcard has been updated"]
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [self.cardSavedAlert show];
        }
        else
        {
            [[PFAPostCardStore sharedStore] addPostcard:self.postcard];
            self.cardSavedAlert = [[UIAlertView alloc]  initWithTitle:[NSString stringWithFormat:@"Postcard saved"]
                                                              message:[NSString stringWithFormat:@"Your postcard has been saved"]
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [self.cardSavedAlert show];
        }
    }
}

- (IBAction)facebookButtonPressed:(UIButton *)sender
{
    SLComposeViewController *SLComposerSheetFacebook = [[SLComposeViewController alloc] init];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) //check if Facebook Account is linked
    {
        SLComposerSheetFacebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        if (self.postcard.artistImage)
        {
            [SLComposerSheetFacebook setInitialText:@"I just mailed a postcard from my phone! http://postcardsfromalaska.com"];
        }
        else
        {
            [SLComposerSheetFacebook setInitialText:@"I just mailed this postcard from my phone! http://postcardsfromalaska.com"];
            [SLComposerSheetFacebook addImage:self.socialImage]; 
        }
        
        [self presentViewController:SLComposerSheetFacebook animated:YES completion:nil];
    }
    [SLComposerSheetFacebook setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everythink worked properly. Give out a message on the state.
    }];
}

- (IBAction)twitterButtonPressed:(UIButton *)sender
{
    SLComposeViewController *SLComposerSheetTwitter = [[SLComposeViewController alloc] init];
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) //check if Twitter Account is linked
    {
        SLComposerSheetTwitter = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        if (self.postcard.artistImage)
        {
            [SLComposerSheetTwitter setInitialText:@"I just mailed a postcard from my phone! http://postcardsfromalaska.com"];
        }
        else
        {
            [SLComposerSheetTwitter setInitialText:@"I just mailed this postcard from my phone! http://postcardsfromalaska.com"];
            [SLComposerSheetTwitter addImage:self.socialImage];
        }
        
        [self presentViewController:SLComposerSheetTwitter animated:YES completion:nil];
    }
    [SLComposerSheetTwitter setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = @"Action Cancelled";
                break;
            case SLComposeViewControllerResultDone:
                output = @"Post Successfull";
                break;
            default:
                break;
        } //check if everythink worked properly. Give out a message on the state.
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (IBAction)flickrButtonPressed:(UIButton *)sender
{
    
}

- (IBAction)tumblrButtonPressed:(UIButton *)sender
{
    
}

- (IBAction)retryButtonPressed:(UIButton *)sender
{
    if (!self.photoUploadedSuccessfully)
    {
        [self animateProgressBarToBottom:NO withError:NO];
        [self hideErrorButton];
        [self sendPostcard:self.postcard];
    }
    else if (!self.paymentComplete)
    {
        [self hideErrorButton];
        [self launchPayPal];
        [[self uploadLabel] setText:@"Checking Payment"];
    }
}

- (void)launchPayPal
{
    // Create a PayPalPayment
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    payment.amount = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%f", self.postcard.price]];
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Postcard";
    
    // Check whether payment is processable.
    if (!payment.processable) {
        // If, for example, the amount was negative or the shortDescription was empty, then
        // this payment would not be processable. You would want to handle that here.
    }
    
    // Start out working with the test environment! When you are ready, remove this line to switch to live.
    [PayPalPaymentViewController setEnvironment:PayPalEnvironmentProduction];
    
    // Provide a payerId that uniquely identifies a user within the scope of your system,
    // such as an email address or user ID.
    NSString *aPayerId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    // Create a PayPalPaymentViewController with the credentials and payerId, the PayPalPayment
    // from the previous step, and a PayPalPaymentDelegate to handle the results.
    PayPalPaymentViewController *paymentViewController;
    paymentViewController = [[PayPalPaymentViewController alloc] initWithClientId:@"Afh4khAAWdB__GMTkaBzVTXimZOWcxOYx4-60hDER567_EFYydX5N8fmR3Fv"
                                                                    receiverEmail:@"richstone@pivo.tl"
                                                                          payerId:aPayerId
                                                                          payment:payment
                                                                         delegate:self];
    
    // Present the PayPalPaymentViewController.
    [self presentViewController:paymentViewController animated:YES completion:nil];

}

#pragma mark - PayPalPaymentDelegate methods

- (void)payPalPaymentDidComplete:(PayPalPayment *)completedPayment {
    // Payment was processed successfully; send to server for verification and fulfillment.
    [self verifyCompletedPayment:completedPayment];
    
    // Dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel {
    // The payment was canceled; dismiss the PayPalPaymentViewController.
    [self dismissViewControllerAnimated:YES completion:nil];
    [self animateProgressBarToBottom:YES withError:YES];
    [self isPaymentComplete:NO];
    [self showErrorButtonWithText:@"Payment"];
}

- (void)verifyCompletedPayment:(PayPalPayment *)completedPayment {
    // Send the entire confirmation dictionary
    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
                                                           options:0
                                                             error:nil];
    
    NSString *paymentData = [[NSString alloc] initWithData:confirmation encoding:NSUTF8StringEncoding];
    
    [self checkPaymentWithData:paymentData];
    
    // Send confirmation to your server; your server should verify the proof of payment
    // and give the user their goods or services. If the server is not reachable, save
    // the confirmation and try again later.
}

- (void)cardOrderComplete
{    
    NSString *orderType = @"User";
    
    if (self.postcard.artistImage)
        orderType = @"Artist";
    
    // google anaylitics
    GAITransaction *transaction =
    [GAITransaction transactionWithId:self.orderNumber            // (NSString) Transaction ID, should be unique.
                      withAffiliation:@"In-App Store"];       // (NSString) Affiliation
     transaction.taxMicros = (int64_t)(0);           // (int64_t) Total tax (in micros)
     transaction.shippingMicros = (int64_t)(0);                   // (int64_t) Total shipping (in micros)
     transaction.revenueMicros = (int64_t)(self.postcard.price * 1000000);       // (int64_t) Total revenue (in micros)
     
     [transaction addItemWithCode:self.orderNumber                         // (NSString) Product SKU
                            name:orderType             // (NSString) Product name
                        category:@"Postcard"               // (NSString) Product category
                     priceMicros:(int64_t)(self.postcard.price * 1000000)        // (int64_t)  Product price (in micros)
                        quantity:1];                              // (NSInteger)  Product quantity
     
     [[GAI sharedInstance].defaultTracker sendTransaction:transaction]; // Send the transaction.
     
    [self animateProgressBarToBottom:YES withError:NO];
    [self isPaymentComplete:YES];
    [self showCompleteButtons];
}

- (void)sendPostcard:(PFAPostCard *)postcard
{    
    // order number set by current time and appended with random number
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMddyyHHmmss"];
    NSInteger randomNumber = arc4random() % 999;
    NSString *date = [formatter stringFromDate:[NSDate date]];
    self.orderNumber = [NSString stringWithFormat:@"%@%d", date, randomNumber];
    
    // setup image url
    if (postcard.artistPath)
    {
        self.imageUrl = postcard.artistPath;
    }
    else
    {
        self.imageUrl = [NSString stringWithFormat:@"%@.jpg", self.orderNumber];
        [self uploadImage:self.postcard.image];
    }
    
    // postcard information
    self.postcardData = [NSString stringWithFormat:@"order_number=%@&from_name=%@&from_email=%@&to_name=%@&to_address1=%@&to_address2=%@&to_city=%@&to_state=%@&to_zip=%@&to_country=%@&message=%@&caption=%@&image_url=%@&price=%f&artist_name=%@&promo=%@&border=%d", self.orderNumber, postcard.sender.name, postcard.sender.email, postcard.recipient.name, postcard.recipient.address1, postcard.recipient.address2, postcard.recipient.city, postcard.recipient.state, postcard.recipient.zip, postcard.recipient.country, postcard.message, postcard.caption, self.imageUrl, postcard.price, postcard.artistName, self.promoCode, self.postcard.hasBorder];
    
    
    if (self.postcard.artistImage)
    {
        [self uploadPostcardData:self.postcardData WithStatus:self.status];
    }

}

- (void)showCompleteButtons
{
    [self.facebookButton setHidden:NO];
    [self.twitterButton setHidden:NO];
    [self.rateAppButton setHidden:NO];
    [self.saveCardButton setHidden:NO];
    [self.returnHome setHidden:NO];
}

- (void)hideCompleteButtons
{
    [self.facebookButton setHidden:NO];
    [self.twitterButton setHidden:NO];
    [self.rateAppButton setHidden:NO];
    [self.saveCardButton setHidden:NO];
    [self.returnHome setHidden:NO];
}

- (void)showErrorButtonWithText:(NSString *)text
{
    [self.retryButton setTitle:[NSString stringWithFormat:@"   Retry %@", text] forState:UIControlStateNormal];
    [self.retryButton setHidden:NO];
    [self.uploadLabel setText:[NSString stringWithFormat:@"%@ failed, please try again", text]];
    [self.cancelToHome setHidden:NO];
    [self.saveCardRetryButton setHidden:NO];
}

- (void)hideErrorButton
{
    [self.retryButton setHidden:YES];
    [self.cancelToHome setHidden:YES];
    [self.saveCardRetryButton setHidden:YES];
}

- (void)animateProgressBarToBottom:(BOOL)bottom withError:(BOOL)error
{
    
    // reset progress color bar
    [self.progressBar setProgressTintColor:[UIColor whiteColor]];
    
    if (bottom)
    {
        if (error)
        {
            [self.progressBar setProgress:1.0];
            [self.progressBar setProgressTintColor:[UIColor redColor]];
        }
        else
        {
            [self.progressBar setProgress:1.0];
            [[self uploadLabel] setText:@"Order complete"];
            [self.retryButton setHidden:YES];
        }
        
        // animate progressbar to bottom
        CGRect frame = self.progressView.frame;
        frame.origin.y = self.view.frame.size.height - self.progressView.frame.size.height - 15;
        
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.progressView.frame = frame;
            [self.uploadLabel setTextColor:[UIColor whiteColor]];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                [self.buttonViewContainer setAlpha:1.0];
            }];
        }];
    }
    else
    {
        if (![self photoUploadedSuccessfully])
        {
            [self.progressBar setProgress:0.0];
            [[self uploadLabel] setText:@"Uploading postcard, please wait..."];
        }
        else
        {
            [self.progressBar setProgress:1.0];
            [[self uploadLabel] setText:@"Checking payment"];
        }
        
        
        // animate progressbar to bottom
        CGRect frame = self.progressView.frame;
        frame.origin.y = 113;
        
        [UIView animateWithDuration:0.1 animations:^{
            [self.buttonViewContainer setAlpha:0.0];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.progressView.frame = frame;
                [self.uploadLabel setTextColor:[UIColor whiteColor]];
            } completion:nil];

        }];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    connection = nil;
    NSString *errorString = [NSString stringWithFormat:@"fetch failed: %@", [error localizedDescription]];
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [av show];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSString *responseText = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    if (connection == self.uploadCardDataConnection)
    {
        if ([responseText isEqualToString:@"YES"])
        {
            if (self.freeCard)
            {
                [self cardOrderComplete];
            }
            else
            {
                [self launchPayPal];
                [[self uploadLabel] setText:@"Checking Payment"];
            }
        }
        else
        {
            [self animateProgressBarToBottom:YES withError:YES];
            [self didPhotoUploadSuccessfully:NO];
            [self showErrorButtonWithText:@"Upload"];
        }
    }
    
    if (connection == self.uploadPaymentData)
    {
        [self cardOrderComplete];
    }
}

- (void)checkPaymentWithData:(NSString *)data
{
    NSString *postData = [NSString stringWithFormat:@"paymentData=%@&order_number=%@", data, self.orderNumber];
    NSURL *url = [NSURL URLWithString:@"https://postcardsfromalaska.com/ntrnl/check-payment.php"];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    self.uploadPaymentData = [[NSURLConnection alloc] initWithRequest:req delegate:self];
}

- (void)uploadPostcardData:(NSString *)data WithStatus:(NSString *)status
{
    [self didPhotoUploadSuccessfully:YES];
    [self.progressBar setProgress:1.0];
    
    NSString *postData = [NSString stringWithFormat:@"%@&status=%@", data, status];
    
    // setup connection for post
    NSURL *url = [NSURL URLWithString:@"https://postcardsfromalaska.com/ntrnl/submit-postcard.php"];
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    self.uploadCardDataConnection = [[NSURLConnection alloc] initWithRequest:req delegate:self];
}

- (void)uploadImage:(UIImage *)image
{
    NSURL *url = [NSURL URLWithString:@"https://postcardsfromalaska.com"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 90);
    
    NSMutableURLRequest *req = [httpClient multipartFormRequestWithMethod:@"POST" path:@"/ntrnl/image-upload.php" parameters:nil constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"file" fileName:self.imageUrl mimeType:@"image/jpeg"];
    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:req];
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        float percentDownloaded = (float) totalBytesWritten / totalBytesExpectedToWrite;
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        [self.progressBar setProgress:percentDownloaded];
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [self uploadPostcardData:self.postcardData WithStatus:self.status];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [self animateProgressBarToBottom:YES withError:YES];
        [self didPhotoUploadSuccessfully:NO];
        [self showErrorButtonWithText:@"Upload"];
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"error"
                                                        withAction:@"upload error"
                                                         withLabel:[NSString stringWithFormat:@"%@", error]
                                                         withValue:[NSNumber numberWithInt:100]];
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Upload Error" message:@"Please check your internet connection and try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
    }];
    
    [operation start];
    
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeLeft;
}

@end

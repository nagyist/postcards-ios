//
//  PFAArtistGalleryScrollViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/4/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "GAITrackedViewController.h"

@interface PFAArtistGalleryScrollViewController : GAITrackedViewController <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *artistGalleryScrollView;
@property (nonatomic) int selectedIndex;
@property (nonatomic, strong) NSString *category;

@end

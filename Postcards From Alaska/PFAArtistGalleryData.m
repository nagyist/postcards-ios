//
//  PFAArtistGalleryData.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) 
#define kArtistGalleryJSONURL [NSURL URLWithString: @"https://postcardsfromalaska.com/ntrnl/postcard_data/artist_gallery.json"] 

#import "PFAArtistGalleryData.h"
#import "PFAArtistImage.h"
#import "PFAArtistGallerySharedStore.h"

@interface PFAArtistGalleryData()

@property (nonatomic, strong) NSURLConnection *theConnection;
@property (nonatomic, strong) NSMutableArray *mutableCategories;
@property (nonatomic, strong) NSMutableDictionary *mutableGalleries;

@end

@implementation PFAArtistGalleryData

- (id)init
{
    self = [super init];
    
    if (self)
    {
                
    }
    
    return self;
}

- (void)loadData
{
    dispatch_async(kBgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:
                        kArtistGalleryJSONURL];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data waitUntilDone:YES];
    });

}

- (NSMutableArray *)mutableCategories
{
    if (!_mutableCategories)
    {
        _mutableCategories = [[NSMutableArray alloc] init];
    }
    
    return _mutableCategories;
}

- (NSMutableDictionary *)mutableGalleries
{
    if (!_mutableGalleries)
    {
        _mutableGalleries = [[NSMutableDictionary alloc] init];
    }
    
    return _mutableGalleries;
}

- (NSArray *)categories
{
    return _mutableCategories;
}

- (NSDictionary *)galleries
{
    return _mutableGalleries;
}

- (void)fetchedData:(NSData *)responseData {
    
    if ([self isNetworkAvailable])
    {
        //parse out the json data
        NSError* error;
        NSDictionary* json = [NSJSONSerialization
                              JSONObjectWithData:responseData //1
                              
                              options:kNilOptions
                              error:&error];
        
        NSArray *pictures = [json objectForKey:@"pictures"];
        
        // insert JSON into array of titles and dictionary of galleries
        for (NSDictionary *d in pictures)
        {
            
            [self.mutableCategories addObject:[d objectForKey:@"title"]];
            
            NSMutableArray *images = [[NSMutableArray alloc] init];
            
            for (NSDictionary *a in [d objectForKey:@"gallery"])
            {
                PFAArtistImage *image = [[PFAArtistImage alloc] initWithPath:[a objectForKey:@"path"] thumbPath:[a objectForKey:@"thumbPath"] caption:[a objectForKey:@"caption"] artistName:[a objectForKey:@"artist"]];
                [images addObject:image];
            }
            
            [self.mutableGalleries setObject:images forKey:[d objectForKey:@"title"]];
            
        }
        
        [[PFAArtistGallerySharedStore sharedStore] setGalleries:self.mutableGalleries];
        [[PFAArtistGallerySharedStore sharedStore] setCategories:self.mutableCategories];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"This app requires a data connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!\n");
        return YES;
    }
}

@end

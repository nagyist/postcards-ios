//
//  PFAArtistGalleryData.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <unistd.h>
#include <netdb.h>

@interface PFAArtistGalleryData : NSObject

@property (nonatomic, strong) NSDictionary *galleries;
@property (nonatomic, strong) NSArray *categories;

- (void)loadData;

@end

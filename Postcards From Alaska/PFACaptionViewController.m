//
//  PFACaptionViewController.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/7/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFACaptionViewController.h"
#import "PFAPostCard.h"
#import "PFAEditorViewController.h"
#import "PFASender.h"

@interface PFACaptionViewController ()
@property (nonatomic, strong) PFAEditorViewController *editorViewController;
@property (nonatomic, strong) PFAPostCard *postcard;
@end

@implementation PFACaptionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // google anaylitics
    self.trackedViewName = @"Caption Input Screen";
	
    self.editorViewController = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];
    
    self.postcard = self.editorViewController.postcard;
    
    [self.photographersName setText:self.postcard.captionName];
    [self.pictureLocation setText:self.postcard.captionLocation];
    
    [self.photographersName becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender
{
        
    [self.postcard setCaptionLocation:self.pictureLocation.text];
    [self.postcard setCaptionName:self.photographersName.text];
    
    [self.editorViewController setPostcard:self.postcard];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end

//
//  PFASender.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/2/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFASender.h"

@implementation PFASender

- (id)init
{
    self = [super init];
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.email forKey:@"email"];
}

@end

//
//  PFAHomeViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/2/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PFAHomeViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIButton *createCardButton;
@property (weak, nonatomic) IBOutlet UIButton *saveCardsButton;
@property (weak, nonatomic) IBOutlet UIImageView *borderBackgroundImageView;

- (IBAction)newCardButtonPressed:(UIButton *)sender;
- (IBAction)savedCardButtonPressed:(UIButton *)sender;

@end

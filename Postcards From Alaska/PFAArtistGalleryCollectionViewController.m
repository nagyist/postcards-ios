//
//  PFAArtistGalleryCollectionViewController.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFAArtistGalleryCollectionViewController.h"
#import "PFAArtistGalleryScrollViewController.h"
#import "PFAArtistGallerySharedStore.h"
#import "PFAArtistImage.h"

@interface PFAArtistGalleryCollectionViewController ()
{
    CGRect screenBound;
    CGSize screenSize;
}
@property (nonatomic) int selectedIndex;
@property (nonatomic, strong) NSArray *gallery;
@end

@implementation PFAArtistGalleryCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = self.category;
    self.gallery = [[NSArray alloc] initWithArray:[[[PFAArtistGallerySharedStore sharedStore] galleries] objectForKey:self.category]];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    screenBound = screenBound = [[UIScreen mainScreen] bounds];
    screenSize = screenSize = screenBound.size;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PFAArtistGalleryScrollViewController *asvc = segue.destinationViewController;
    [asvc setCategory:self.category];
    
    NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
    NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
    [asvc setSelectedIndex:indexPath.row];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.gallery count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"artistGalleryCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    UIImageView *galleryImageView = (UIImageView *)[cell viewWithTag:100];
    
    PFAArtistImage *image = [self.gallery objectAtIndex:[indexPath row]];
    
    NSString *thumbPath = image.thumbPath;
    
    if (isRetina)
    {
        thumbPath = [thumbPath stringByReplacingOccurrencesOfString:@".jpg" withString:@"@2x.jpg"];
    }
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center = galleryImageView.center;
    
    [galleryImageView addSubview:activityView];
    
    [activityView startAnimating];
    
    [galleryImageView setImageWithURL:[NSURL URLWithString:thumbPath] placeholderImage:[UIImage imageNamed:@"placeholder-artistgallery.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        [activityView stopAnimating];
    }];
    
    return cell; 
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end

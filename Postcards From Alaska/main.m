//
//  main.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/2/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PFAAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PFAAppDelegate class]));
    }
}

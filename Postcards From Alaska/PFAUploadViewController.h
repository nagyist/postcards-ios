//
//  PFAUploadViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/13/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import "GAITrackedViewController.h"

@class PFAPostCard;

@interface PFAUploadViewController : GAITrackedViewController <UIAlertViewDelegate>

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) PFAPostCard *postcard;
@property (nonatomic, strong) NSString *promoCode;
@property (nonatomic) BOOL isSavedCard;
@property (nonatomic, setter = isFreeCard:) BOOL freeCard;
@property (nonatomic) NSInteger savedCardIndex;
@property (nonatomic, strong) NSString *status;

@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UILabel *uploadLabel;
@property (weak, nonatomic) IBOutlet UIView *progressView;

@property (weak, nonatomic) IBOutlet UIButton *rateAppButton;
@property (weak, nonatomic) IBOutlet UIButton *returnHome;
@property (weak, nonatomic) IBOutlet UIButton *cancelToHome;
@property (weak, nonatomic) IBOutlet UIView *buttonViewContainer;
@property (weak, nonatomic) IBOutlet UIButton *saveCardButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;
@property (weak, nonatomic) IBOutlet UIButton *saveCardRetryButton;

@property (weak, nonatomic) IBOutlet UIButton *retryButton;

- (IBAction)rateAppPressed:(UIButton *)sender;
- (IBAction)returnHomePressed:(UIButton *)sender;
- (IBAction)saveCardButtonPressed:(UIButton *)sender;
- (IBAction)retryButtonPressed:(UIButton*)sender;

- (IBAction)facebookButtonPressed:(UIButton *)sender;
- (IBAction)twitterButtonPressed:(UIButton *)sender;
- (IBAction)flickrButtonPressed:(UIButton *)sender;
- (IBAction)tumblrButtonPressed:(UIButton *)sender;

- (void)sendPostcard :(PFAPostCard *)postcard;

@end

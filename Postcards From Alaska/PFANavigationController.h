//
//  PFANavigationController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/8/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFANavigationController : UINavigationController

@end

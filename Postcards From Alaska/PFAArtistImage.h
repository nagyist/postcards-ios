//
//  PFAArtistImage.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFAArtistImage : NSObject

@property (nonatomic, retain) NSString *path;
@property (nonatomic, retain) NSString *thumbPath;
@property (nonatomic, retain) NSString *caption;
@property (nonatomic, retain) NSString *artistPath;
@property (nonatomic, retain) NSString *artistName;

- (id)initWithPath :(NSString *)path thumbPath:(NSString *)thumbPath caption:(NSString *)caption artistName:(NSString *)artistName;
@end

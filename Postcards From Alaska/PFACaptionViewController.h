//
//  PFACaptionViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/7/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface PFACaptionViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UITextField *photographersName;
@property (weak, nonatomic) IBOutlet UITextField *pictureLocation;



- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender;

@end

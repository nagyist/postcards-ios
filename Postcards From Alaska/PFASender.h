//
//  PFASender.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/2/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PFASender : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

@end

//
//  util.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface util : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (void)shakeView:(UIView *)viewToShake;

@end

//
//  PFAPurchaseViewController.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/4/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFAPurchaseViewController.h"
#import "PFAPostCard.h"
#import "PFAEditorViewController.h"
#import "PFAUploadViewController.h"
#import "GAI.h"
#import "util.h"

@interface PFAPurchaseViewController ()

@property (nonatomic, strong) NSURLConnection *promoCodeConnection;
@property (nonatomic, strong) NSURLConnection *promoUsesConnection;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSString *promoCode;
@property (nonatomic, strong) PFAEditorViewController *editorViewController;
@property (nonatomic) double price;
@property (nonatomic, setter = isPromoCodeActive:) BOOL promoCodeActive;
@property (nonatomic) double originalPrice;
@property (nonatomic, setter = isFreeCard:) BOOL freeCard;

@end

@implementation PFAPurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // google anaylitics
    self.trackedViewName = @"Purchase Screen";
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker sendEventWithCategory:@"uiAction"
                        withAction:@"buttonPress"
                         withLabel:self.promoButton.titleLabel.text
                         withValue:[NSNumber numberWithInt:100]];
    
    // set fonts
    UIFont *buttonFont = [UIFont fontWithName:@"League Gothic" size:24];
    [self.purchaseButton.titleLabel setFont:buttonFont];
    [self.cancelButton.titleLabel setFont:buttonFont];
    [self.promoButton.titleLabel setFont:buttonFont];
    [self.warningLabel setFont:buttonFont];
    
    self.editorViewController = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];
    self.price = self.editorViewController.postcard.price;
    self.originalPrice = self.editorViewController.postcard.price;
    [self.purchaseButton setTitle:[NSString stringWithFormat:@"   Purchase $%.2f", self.price] forState:UIControlStateNormal];
    
    [self isFreeCard:NO];
    [self isPromoCodeActive:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        self.promoCode = [NSString stringWithFormat:@"promo=%@", [[alertView textFieldAtIndex:0] text]];
        NSData *promoCodeData = [self.promoCode dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%d", [promoCodeData length]];
        
        NSURL *url = [NSURL URLWithString:@"https://postcardsfromalaska.com/ntrnl/promos.php"];
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
        [req setHTTPMethod:@"POST"];
        [req setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [req setHTTPBody:promoCodeData];
        self.promoCodeConnection = [[NSURLConnection alloc] initWithRequest:req delegate:self startImmediately:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (connection == self.promoCodeConnection)
    {
        NSString *responseText = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([responseText isEqualToString:@"NO"])
        {
            [util shakeView:self.view];
        }
        else
        {
            self.price = self.originalPrice;
            NSString *discountPrice = [responseText stringByReplacingOccurrencesOfString:@"YES" withString:@""];
            self.price = self.price - [discountPrice doubleValue];
            
            if (self.price > 0)
                [self.purchaseButton setTitle:[NSString stringWithFormat:@"   Purchase $%.2f", self.price] forState:UIControlStateNormal];
            else
            {
                [self.purchaseButton setTitle:@"   Purchase FREE" forState:UIControlStateNormal];
                [self isFreeCard:YES];
            }
            
            [self isPromoCodeActive:YES];
            [self.postcard setPrice:self.price];
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    connection = nil;
    NSString *errorString = [NSString stringWithFormat:@"fetch failed: %@", [error localizedDescription]];
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:errorString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [av show];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PFAUploadViewController *uvc = segue.destinationViewController;
    [uvc setPostcard:self.postcard];
    [uvc setPromoCode:self.promoCode];
    
    if (self.freeCard)
        [uvc isFreeCard:YES];
    else
        [uvc isFreeCard:NO];
    
    if (self.isSavedCard)
    {
        [uvc setSavedCardIndex:self.savedCardIndex];
        [uvc setIsSavedCard:YES];
    }
    else
    {
        [uvc setIsSavedCard:NO];
    }
}

- (IBAction)promoButtonPressed:(UIButton *)sender
{
    self.promoCodeAlertView = [[UIAlertView alloc] initWithTitle:@"Promo Code" message:@"Please enter your promo code below" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
    
    [self.promoCodeAlertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        
    [self.promoCodeAlertView show];
}

- (IBAction)cancelButtonPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)purchaseButtonPressed:(UIButton *)sender
{
    
    if (self.promoCodeActive)
    {
        NSData *promoCodeData = [self.promoCode dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%d", [promoCodeData length]];
        
        NSURL *url = [NSURL URLWithString:@"https://postcardsfromalaska.com/ntrnl/promo-count.php"];
        NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
        [req setHTTPMethod:@"POST"];
        [req setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [req setHTTPBody:promoCodeData];
        self.promoUsesConnection = [[NSURLConnection alloc] initWithRequest:req delegate:self startImmediately:YES];
    }

}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end

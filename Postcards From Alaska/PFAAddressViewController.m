//
//  PFAAddressViewController.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/5/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFAAddressViewController.h"
#import "PFAEditorViewController.h"
#import "PFARecipient.h"
#import "PFAPostCard.h"

@interface PFAAddressViewController ()
@property (nonatomic, strong) PFAEditorViewController *editorViewController;
@property (nonatomic, strong) NSArray *textFieldsArray;
@property (nonatomic, strong) UITextField *activeField;
@property (nonatomic) BOOL allFieldsFilled;
@property (nonatomic, setter = isNewContact:) BOOL newContact;
@property (nonatomic, strong) PFAPostCard *postcard;
@property (nonatomic, strong) UIAlertView *contactExistAlert;
@property (nonatomic) ABRecordRef newPerson;
@property (nonatomic) ABRecordRef updatePersonRef;
@end

@implementation PFAAddressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // google anaylitics
    self.trackedViewName = @"Address Input Screen";
    
    self.textFieldsArray = [[NSArray alloc] initWithObjects:self.toTextField, self.address1TextField, self.cityTextField, self.zipTextField, nil];
	
    self.editorViewController = [self.navigationController.viewControllers objectAtIndex:[self.navigationController.viewControllers count] - 2];
    
    self.postcard = self.editorViewController.postcard;
    // set text fields to postcard data if available
    [self.toTextField setText:self.postcard.recipient.name];
    [self.address1TextField setText:self.postcard.recipient.address1];
    [self.address2TextField setText:self.postcard.recipient.address2];
    [self.cityTextField setText:self.postcard.recipient.city];
    [self.stateTextField setText:self.postcard.recipient.state];
    [self.zipTextField setText:self.postcard.recipient.zip];
    if (self.postcard.recipient.country != NULL)
    {
        [self.countryTextField setText:self.postcard.recipient.country];
    }
    
    self.allFieldsFilled = NO;
    
    // tap gesture
    self.singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:self.singleTap];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // style contacts bar button
    self.contactsBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contactsBarButton setBackgroundImage:[[UIImage imageNamed:@"barButton.png"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0] forState:UIControlStateNormal];
    [self.contactsBarButton setTitle:@"View Contacts" forState:UIControlStateNormal];
    self.contactsBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    self.contactsBarButton.titleLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
	self.contactsBarButton.titleLabel.shadowOffset = CGSizeMake(0, -1);
    self.contactsBarButton.frame = CGRectMake((self.navigationController.navigationBar.frame.size.width / 2) - 105, 4.0, 100.0, 24.0);
    [self.contactsBarButton addTarget:self action:@selector(contactsBarButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.contactsBarButton setTag:10];
    
    [self.navigationController.navigationBar addSubview:self.contactsBarButton];
    
    // style add contacts bar button
    self.addContactsBarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addContactsBarButton setBackgroundImage:[[UIImage imageNamed:@"barButton.png"] stretchableImageWithLeftCapWidth:5.0 topCapHeight:0.0] forState:UIControlStateNormal];
    [self.addContactsBarButton setTitle:@"Save Contact" forState:UIControlStateNormal];
    self.addContactsBarButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    self.addContactsBarButton.titleLabel.shadowColor = [UIColor colorWithWhite:0 alpha:0.5];
	self.addContactsBarButton.titleLabel.shadowOffset = CGSizeMake(0, -1);
    self.addContactsBarButton.frame = CGRectMake((self.navigationController.navigationBar.frame.size.width / 2) + 5, 4.0, 90.0, 24.0);
    [self.addContactsBarButton addTarget:self action:@selector(addContactsBarButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.addContactsBarButton setTag:10];
    
    [self.navigationController.navigationBar addSubview:self.addContactsBarButton];
    
    // set country from country table view
    if (self.countryFromTableView != nil)
        [self.countryTextField setText:self.countryFromTableView];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // remove contact view from navigtion bar
    [self.contactsBarButton removeFromSuperview];
    [self.addContactsBarButton removeFromSuperview];
}

- (void)testFields
{
    for (int i = 0 ; i < [self.textFieldsArray count]; i++)
    {
        UITextField *activeTextField = [self.textFieldsArray objectAtIndex:i];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information"
                                                        message:[NSString stringWithFormat:@"%@ field is missing information", activeTextField.placeholder]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        if ([activeTextField.text isEqualToString:@""] || activeTextField.text == NULL)
        {
            [activeTextField becomeFirstResponder];
            [alert show];
            self.allFieldsFilled = NO;
            break;
        }
        
        self.allFieldsFilled = YES;
    
    }
}

- (void)peoplePickerNavigationControllerDidCancel:(ABPeoplePickerNavigationController *)peoplePicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
    [self displayPerson:person];
    
    return YES;
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker shouldContinueAfterSelectingPerson:(ABRecordRef)person property:(ABPropertyID)property identifier:(ABMultiValueIdentifier)identifier
{
    // Only inspect the value if it's an address.
    if (property == kABPersonAddressProperty) {
        /*
         * Set up an ABMultiValue to hold the address values; copy from address
         * book record.
         */
        ABMultiValueRef multi = ABRecordCopyValue(person, property);
        
        // Set up an NSArray and copy the values in.
        NSArray *theArray = (__bridge id)ABMultiValueCopyArrayOfAllValues(multi);
        
        // Figure out which values we want and store the index.
        const NSUInteger theIndex = ABMultiValueGetIndexForIdentifier(multi, identifier);
        
        // Set up an NSDictionary to hold the contents of the array.
        NSDictionary *theDict = [theArray objectAtIndex:theIndex];
        
        NSString *address = [theDict objectForKey:(NSString *)kABPersonAddressStreetKey];
        NSArray *addressArray = [address componentsSeparatedByString:@"\n"];
        
        if ([addressArray count] > 1)
        {
            [self.address1TextField setText:[addressArray objectAtIndex:0]];
            [self.address2TextField setText:[addressArray objectAtIndex:1]];
        }
        else
        {
            [self.address1TextField setText:address];
            [self.address2TextField setText:@""];
        }
        
        [self.cityTextField setText:[theDict objectForKey:(NSString *)kABPersonAddressCityKey]];
        [self.stateTextField setText:[theDict objectForKey:(NSString *)kABPersonAddressStateKey]];
        [self.zipTextField setText:[theDict objectForKey:(NSString *)kABPersonAddressZIPKey]];
        [self setCountryFromTableView:[theDict objectForKey:(NSString *)kABPersonAddressCountryKey]];
        
        NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
        NSString *lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
        
        if (lastName == NULL)
            lastName = @"";
        
        [self.toTextField setText:[NSString stringWithFormat:@"%@ %@", firstName, lastName]];
        
        // Return to the main view controller.
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    
    // If they didn't pick an address, return YES here to keep going.
    return NO;
}

- (void)displayPerson:(ABRecordRef)person
{
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)gr
{
    CGPoint location = [gr locationInView:self.scrollView];
    
    if (CGRectContainsPoint(self.countryTextField.frame, location))
    {
        [self performSegueWithIdentifier:@"countryTableSegue" sender:self];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (IBAction)doneButtonPressed:(UIBarButtonItem *)sender
{
    
    [self testFields];

    if (self.allFieldsFilled)
    {
        // set address 2 to blank if not filled for proper address formatting on card
        if (self.address2TextField.text == NULL || [self.address2TextField.text length] < 1)
            self.address2TextField.text = @"";
        
        PFARecipient *recipient = [[PFARecipient alloc] init];
        [recipient setName:self.toTextField.text];
        [recipient setAddress1:self.address1TextField.text];
        [recipient setAddress2:self.address2TextField.text];
        [recipient setCity:self.cityTextField.text];
        [recipient setState:self.stateTextField.text];
        [recipient setZip:self.zipTextField.text];
        [recipient setCountry:self.countryTextField.text];
        
        [self.postcard setRecipient:recipient];
        
        [self.editorViewController setPostcard:self.postcard];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)addContactsBarButtonPressed
{
    [self testFields];
    
    if (self.allFieldsFilled)
    {    
        // create new entry
        CFErrorRef error;
        self.newPerson = ABPersonCreate();
        
        NSArray *nameArray = [self.toTextField.text componentsSeparatedByString:@" "];
        NSString *addressString = [NSString stringWithFormat:@"%@\n%@", self.address1TextField.text, self.address2TextField.text];
        
        // set values
        if ([nameArray count] > 0)
            ABRecordSetValue(self.newPerson, kABPersonFirstNameProperty, (__bridge CFTypeRef)([nameArray objectAtIndex:0]), &error);
        if([nameArray count] > 1)
            ABRecordSetValue(self.newPerson, kABPersonLastNameProperty, (__bridge CFTypeRef)([nameArray objectAtIndex:1]), &error);
        
        ABMutableMultiValueRef addressMultiValue = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
        NSMutableDictionary *addressDictionary = [[NSMutableDictionary alloc] init];
        [addressDictionary setObject:addressString forKey:(NSString *)kABPersonAddressStreetKey];
        [addressDictionary setObject:self.cityTextField.text forKey:(NSString *)kABPersonAddressCityKey];
        [addressDictionary setObject:self.stateTextField.text forKey:(NSString *)kABPersonAddressStateKey];
        [addressDictionary setObject:self.countryTextField.text forKey:(NSString *)kABPersonAddressCountryCodeKey];
        [addressDictionary setObject:self.zipTextField.text forKey:(NSString *)kABPersonAddressZIPKey];
        ABMultiValueAddValueAndLabel(addressMultiValue, (__bridge CFTypeRef)(addressDictionary), kABHomeLabel, NULL);
        ABRecordSetValue(self.newPerson, kABPersonAddressProperty, addressMultiValue, nil);
        
        ABUnknownPersonViewController *picker = [[ABUnknownPersonViewController alloc] init];
        picker.unknownPersonViewDelegate = self;
        [picker setDisplayedPerson:self.newPerson];
        [picker setAllowsAddingToAddressBook:YES];
        [self.navigationController pushViewController:picker animated:YES];
    }
    
}

- (void)unknownPersonViewController:(ABUnknownPersonViewController *)unknownCardViewController didResolveToPerson:(ABRecordRef)person
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person
{
    CFErrorRef error;
    if (person)
    {
        ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(nil, &error);
        ABAddressBookAddRecord(addressBook, person, nil);
        ABAddressBookSave(addressBook, nil);
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}


- (void)contactsBarButtonPressed
{
    ABPeoplePickerNavigationController *picker = [[ABPeoplePickerNavigationController alloc] init];
    picker.peoplePickerDelegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end

//
//  PFAArtistGalleryTableViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/3/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PFAArtistGalleryData;

@interface PFAArtistGalleryTableViewController : UITableViewController

@property (nonatomic, strong) PFAArtistGalleryData *artistGalleryData;

@end

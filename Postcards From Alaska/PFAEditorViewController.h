//
//  PFAEditorViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/2/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PFAPostCard.h"
#import "GAITrackedViewController.h"

@interface PFAEditorViewController : GAITrackedViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate>

@property (nonatomic, strong) PFAPostCard *postcard;

@property (nonatomic, strong) IBOutlet UIView *changeImagePopupView;
@property (nonatomic, strong) UIView *changeImagePopupBackgroundView;
@property (weak, nonatomic) IBOutlet UIView *postcardFrontView;
@property (weak, nonatomic) IBOutlet UIView *postcardBackView;
@property (weak, nonatomic) IBOutlet UIView *postcardView;
@property (weak, nonatomic) IBOutlet UIView *grayBackground;
@property (strong, nonatomic) UIView *borderView;

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UILabel *addressNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressStreetLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressCityStateZipLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressCountryLabel;

@property (nonatomic, strong) UIAlertView *saveCardAlert;
@property (nonatomic, strong) UIAlertView *noCaptionAllowedAlert;
@property (nonatomic, strong) UIAlertView *noCameraAlert;
@property (nonatomic, strong) UIAlertView *cardCheckAlert;
@property (nonatomic, strong) UIAlertView *updateCardAlert;

@property (strong, nonatomic) UIImageView *postcardBackImageView;

@property (nonatomic, strong) UITapGestureRecognizer *singleTap;

@property (nonatomic, strong) UIImagePickerController *imagePicker;

@property (weak, nonatomic) IBOutlet UIButton *takePictureButton;
@property (weak, nonatomic) IBOutlet UIButton *userGalleryButton;
@property (weak, nonatomic) IBOutlet UIButton *artistGalleryButton;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (weak, nonatomic) IBOutlet UIButton *borderButton;
@property (weak, nonatomic) IBOutlet UIButton *captionButton;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *flipButton;

@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;

@property (nonatomic, strong) NSString *yearString;

@property (nonatomic) BOOL isSavedCard;
@property (nonatomic) NSInteger savedCardIndex;

- (void)changeImage;
- (UIImage *)setupImageForPostcardBack :(UIImage *)image;

- (IBAction)closeButtonPressed:(UIButton *)sender;
- (IBAction)borderButtonPressed:(UIButton *)sender;
- (IBAction)changeImageButtonPressed:(UIButton *)sender;
- (IBAction)takePictureButtonPressed:(UIButton *)sender;
- (IBAction)userGalleryButtonPressed:(UIButton *)sender;
- (IBAction)homeButtonPressed:(UIButton *)sender;
- (IBAction)artistGalleryButtonPressed:(UIButton *)sender;
- (IBAction)flipCardButton:(UIButton *)sender;
- (IBAction)captionButtonPressed:(UIButton *)sender;
- (IBAction)purchaseButtonPressed:(UIButton *)sender;

@end

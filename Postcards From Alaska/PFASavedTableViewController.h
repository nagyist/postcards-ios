//
//  PFASavedTableViewController.h
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/10/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PFASavedTableViewController : UITableViewController

- (IBAction)toggleEditMode:(UIBarButtonItem *)sender;

@end

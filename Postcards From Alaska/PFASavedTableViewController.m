//
//  PFASavedTableViewController.m
//  Postcards From Alaska
//
//  Created by Chris Hustman on 5/10/13.
//  Copyright (c) 2013 Chris Hustman. All rights reserved.
//

#import "PFASavedTableViewController.h"
#import "PFAPostCardStore.h"
#import "PFAPostCard.h"
#import "PFARecipient.h"
#import "PFASender.h"
#import "PFAEditorViewController.h"
#import "util.h"

@interface PFASavedTableViewController ()

@end

@implementation PFASavedTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[[PFAPostCardStore sharedStore] allPostcards] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"savedCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    PFAPostCard *postcard = [[[PFAPostCardStore sharedStore] allPostcards] objectAtIndex:[indexPath row]];
    
    UIImageView *cellImageView = (UIImageView *)[cell viewWithTag:100];
    UILabel *toLabel = (UILabel *)[cell viewWithTag:200];
    UILabel *fromLabel = (UILabel *)[cell viewWithTag:300];
    
    NSString *toName = nil;
    NSString *fromName = nil;
    
    if (postcard.sender.name)
        fromName = postcard.sender.name;
    else
        fromName = @"No Name Entered";
    
    if (postcard.recipient.name)
        toName = postcard.recipient.name;
    else
        toName = @"No Name Entered";
    
    [toLabel setText:[NSString stringWithFormat:@"To: %@", toName]];
    [fromLabel setText:[NSString stringWithFormat:@"From: %@", fromName]];
    
    [cellImageView setImage:[self setupImage:postcard.image]];
    
    return cell;
}

- (UIImage *)setupImage :(UIImage *)image
{
    
    /**
     *  This mess fixes images taken with the camera not rotating
     */
    
    // find the proportion scale of the image
    float scale = 1.0;
    
    if (image.size.width < image.size.height)
        scale = image.size.width / image.size.height;
    else
        scale = image.size.height / image.size.width;
    
    // set max size to 420 and scale other side
    CGSize landscapeScale = CGSizeMake(105, scale * 105);
    CGSize portraitScale = CGSizeMake(scale * 105, 105);
    
    if (isRetina)
    {
        landscapeScale = CGSizeMake(landscapeScale.width * 2, landscapeScale.height * 2);
        portraitScale = CGSizeMake(portraitScale.width * 2, portraitScale.height * 2);
    }
    
    UIImage *resizeImage = nil;
    
    if (image.size.width < image.size.height)
    {
        UIImage *portraitImage = [util imageWithImage:image scaledToSize:portraitScale];
        resizeImage = [[UIImage alloc] initWithCGImage:portraitImage.CGImage scale:1.0 orientation:UIImageOrientationRight];
        portraitImage = nil;
    }
    else
    {
        resizeImage = [util imageWithImage:image scaledToSize:landscapeScale];
    }
    
    return resizeImage;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        PFAPostCardStore *ps = [PFAPostCardStore sharedStore];
        NSArray *postcards = [ps allPostcards];
        PFAPostCard *postcard = [postcards objectAtIndex:[indexPath row]];
        [ps deletePostcard:postcard];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self performSegueWithIdentifier:@"savedEditorViewSegue" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PFAEditorViewController *evc = segue.destinationViewController;
    NSInteger index = [[self.tableView indexPathForSelectedRow] row];
    
    [evc setIsSavedCard:YES];
    [evc setSavedCardIndex:index];
}

- (IBAction)toggleEditMode:(UIBarButtonItem *)sender
{
    if ([self isEditing])
    {
        [sender setTitle:@"Edit"];
        [self setEditing:NO animated:YES];
    }
    else
    {
        [sender setTitle:@"Done"];
        [self setEditing:YES animated:YES];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}
@end
